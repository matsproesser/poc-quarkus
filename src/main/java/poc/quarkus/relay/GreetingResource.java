package poc.quarkus.relay;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import poc.quarkus.fw.Exchange;
import poc.quarkus.fw.MongoInsert;
import poc.quarkus.fw.Queue;
import rx.Observable;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Random;
import java.util.logging.Logger;

@Path("/hello")
public class GreetingResource {

    private Logger log = Logger.getLogger(this.getClass().getName());

    private Exchange<String> blaExchange;
    private Queue<String> blaQueue;
    private MongoInsert<MessageVo> mongoInsert;
    private String lastMessage;

    public GreetingResource(@ConfigProperty(name="amqp-connection-uri") String connectionUri, @ConfigProperty(name="mongo-connection-uri") String mongoConnectionUri)  {
        blaExchange = new Exchange(connectionUri, "bla", "");
        blaQueue = new Queue(connectionUri, "bla", String.class);
        mongoInsert = new MongoInsert(mongoConnectionUri, "mongoBla", "blaCollection");

        blaQueue.getObservable().subscribe(message -> {
            log.info("new message arrived: " + message.getPayload());
            try {
                mongoInsert.insert(new MessageVo(message.getPayload(), System.currentTimeMillis()), () -> {message.ack(); return null;});
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            lastMessage = message.getPayload();
        });
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@Valid @Min(4) @QueryParam("msg") String msg) throws JsonProcessingException {
        blaExchange.send(msg);
        return "enviado";
    }

    @GET
    @Path("/watch")
    public String watchQueue() {
        return lastMessage;
    }
}