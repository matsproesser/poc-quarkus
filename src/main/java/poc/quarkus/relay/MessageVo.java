package poc.quarkus.relay;

public class MessageVo {

    private String message;
    private Long randuint;

    public MessageVo(String message, Long randuint) {
        this.message = message;
        this.randuint = randuint;
    }

    public String getMessage() {
        return message;
    }

    public Long getRanduint() {
        return randuint;
    }
}
