package poc.quarkus.fw;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.reactivestreams.client.*;
import org.bson.Document;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import rx.functions.Func0;
import rx.internal.operators.OperatorSubscribeOn;

import java.util.logging.Logger;

public class MongoInsert<T> {

    private static final Logger log = Logger.getLogger(MongoInsert.class.getName());


    private final MongoDatabase database;
    private final MongoCollection<Document> collection;
    private ObjectMapper mapper = new ObjectMapper();

    public MongoInsert(String connectionString, String databaseName, String collectionName) {
        MongoClient mongoClient = MongoClients.create(connectionString);
        database = mongoClient.getDatabase(databaseName);
        collection = database.getCollection(collectionName);

    }

    public void insert(T doc, Func0<Void> callback) throws JsonProcessingException {
        collection.insertOne(Document.parse(mapper.writeValueAsString(doc))).subscribe(new Subscriber<Success>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(1L);
            }

            @Override
            public void onNext(Success success) {
                log.info("message saved");
                callback.call();
            }

            @Override
            public void onError(Throwable t) {
                log.warning("message error:");
                t.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
