package poc.quarkus.fw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meltwater.rxrabbit.*;
import com.meltwater.rxrabbit.impl.DefaultChannelFactory;
import rx.Observable;
import rx.Observer;

import java.io.IOException;
import java.util.logging.Logger;

public class Queue<T> {

    private final Observable<Message> consumer;
    private ObjectMapper mapper = new ObjectMapper();
    private Class<T> clazz;
    private Logger log = Logger.getLogger(Queue.class.getName());

    public Queue( String connectionUri, String queueName, Class<T> classtype) {
        clazz = classtype;
        ChannelFactory channelFactory = new DefaultChannelFactory(new BrokerAddresses(connectionUri),new ConnectionSettings());
        ConsumerSettings settings = new ConsumerSettings();
        settings.withConsumerTagPrefix("fwQueue");
        ConsumerFactory consumerFactory = new DefaultConsumerFactory(channelFactory, settings);
        consumer = consumerFactory.createConsumer(queueName);
    }

    public Observable<MessageWrapper<T>> getObservable() {

        return consumer.map(message -> {
            try {
                return new MessageWrapper<T>(mapper.readValue(message.payload, clazz), () -> {
                    message.acknowledger.ack();
                    return null;
                });
            } catch (IOException e) {
                log.warning(e.getLocalizedMessage());
                return null;
            }
        });
    }
}
