package poc.quarkus.fw;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meltwater.rxrabbit.*;
import com.meltwater.rxrabbit.impl.DefaultChannelFactory;

import java.util.logging.Logger;


public class Exchange<T> {

    private Logger log = Logger.getLogger(Exchange.class.getName());
    private final RabbitPublisher publisher;
    private final com.meltwater.rxrabbit.Exchange exchange;
    private final RoutingKey routingKey;
    private ObjectMapper mapper = new ObjectMapper();


    public Exchange(String connectionUri, String exchangeName, String routingKeyName) {
        exchange = new com.meltwater.rxrabbit.Exchange(exchangeName);
        routingKey = new RoutingKey(routingKeyName);
        ChannelFactory channelFactory = new DefaultChannelFactory(new BrokerAddresses(connectionUri), new ConnectionSettings());
        PublisherFactory publisherFactory = new DefaultPublisherFactory(channelFactory, new PublisherSettings());
         publisher = publisherFactory.createPublisher();
    }
    public void send(T message) throws JsonProcessingException {
        publisher.call(exchange,routingKey,null,
                new Payload(mapper.writeValueAsString(message).getBytes())).subscribe();
    }
}
