package poc.quarkus.fw;

import com.meltwater.rxrabbit.Acknowledger;
import rx.functions.Func0;
import rx.functions.Function;

public class MessageWrapper<T> {

    private T payload;

    private Func0<Void> callback;

    public MessageWrapper(T payload, Func0<Void> callback) {
        this.payload = payload;
        this.callback = callback;
    }

    public T getPayload() {
        return payload;
    }

    public void ack() {
        callback.call();
    }
}
